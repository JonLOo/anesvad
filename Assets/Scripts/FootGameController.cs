﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FootGameController : MonoBehaviour {
	
	int TOTAL_GAMES = 3;
	
	int currentGame = 0;
	public int GameTime ;
	bool vibrationCanBeCalled = true;
	bool canVibrate = true;
	bool firstTouch = true;
	bool gameFinished = false;
	bool endMenuVisible = false;
	float mediumSens = 1.5f;
	float fullSens = 0.5f;
	float elapsedTime = 0;
	List<GameObject> Patterns = new List<GameObject>(); 
	public tk2dTextMesh timeText;
	public GameObject endMenu;
	public GameObject timer;
	int pattern;
	
	void Start () {
		timeText.text = GameTime.ToString();
		GetRandomPattern ();
	}

	void GetRandomPattern ()
	{
		foreach(Transform child in GameObject.Find("Patterns").transform){
			Patterns.Add(child.gameObject);	
		}
		Random.seed = System.DateTime.Now.Second;
		pattern = 0;
		int randomGoodOrBad = Random.Range(0,3);
		if(randomGoodOrBad == 0){
			pattern = 4;
		}else{
			pattern = Random.Range(0,3);
		}
		Debug.Log("Pattern: "+ pattern);
		GameObject patternGameObject = Patterns[pattern];
		foreach(Transform child in patternGameObject.transform){
			child.renderer.enabled = false;
		}
		patternGameObject.SetActive(true);
	}



	void RestartGame(){
	
		
		
		gameFinished = false;
		
		endMenuVisible = false;
		elapsedTime = 0;
		timeText.text = GameTime.ToString();
		GetRandomPattern ();
		timer.SetActive(true);
	 	vibrationCanBeCalled = true;
		canVibrate = true;
		firstTouch = true;
		
		
	}

	void Update () {
		elapsedTime += Time.deltaTime;
		int elapsedInt = (int)elapsedTime;
		int showTime = GameTime - elapsedInt;
		
		if(showTime >= 0){
			timeText.text = showTime.ToString();
		}
		if(elapsedTime >= GameTime){
			gameFinished = true;
		}
		if(!gameFinished){
			ManageTouches ();
		}else{
			ManageMenuTouches();
			if(!endMenuVisible && gameFinished){
				endMenuVisible = true;
				ShowEndMenu();
			}
		}
   	}

	void ManageTouches ()
	{
		Vector2 point = new Vector2 (0,0);
		if(Application.platform == RuntimePlatform.OSXEditor ){
			point = Input.mousePosition;
			
		}else{
			point = Input.GetTouch(0).position;
			if(Input.touches.Length == 0){
				vibrationCanBeCalled = false;
				canVibrate = false;
				firstTouch = true;
			}
			
		}
		Ray ray = Camera.main.ScreenPointToRay (point);
		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast (ray, out hit)){
	    	FootTouched (hit);
	    }
	}
	
	void ManageMenuTouches(){
		Debug.Log("touch menu");
		Vector2 point = new Vector2 (0,0);
		if(Application.platform == RuntimePlatform.OSXEditor ){
			point = Input.mousePosition;
			
		}else{
			point = Input.GetTouch(0).position;
		}
			Ray ray = Camera.main.ScreenPointToRay (point);
			RaycastHit hit = new RaycastHit();
			if (Physics.Raycast (ray, out hit)){
		    	if(hit.collider.name == "SI"){
		      		PressedYes();	
		      	}else if(hit.collider.name == "NO"){
		      		PressedNo();
		    	}
			}
	}
	
   	void FootTouched (RaycastHit hit){
		if(hit.collider.name != "NONSensitivity" ){
			canVibrate = true;
		     	if(vibrationCanBeCalled){
		      		vibrationCanBeCalled = false;
		      		if(firstTouch){
		      			firstTouch = false;
		      			Vibrate();
		      		}
		      	if(hit.collider.name == "MediumSensitivity"){
		      		Invoke("Vibrate",mediumSens);
		      	}else{
		      		Invoke("Vibrate",fullSens);
		      	}
		     }
	     }else if(hit.collider.name != "SI" && hit.collider.name != "NO"){
	     	canVibrate = false;
	     	Debug.Log("QUIETO");
	     }
	}
	
   	void Vibrate(){
   		vibrationCanBeCalled = true;
   		if(canVibrate){	
   			Handheld.Vibrate();
	    }
   	}
   	
   	void ShowEndMenu(){
   		timer.SetActive(false);
  	 	endMenu.SetActive(true);
   	}
   	
   	void PressedYes(){
  	 	Debug.Log(currentGame);
  	 	endMenu.SetActive(false);
   		if(pattern == 4){
   			CorrectAnswer();
  	 	}else{
  	 		WrongAnswer();
  	 	}

   	}
   	
   	void PressedNo(){
   		endMenu.SetActive(false);
   		if(pattern != 4){
   			CorrectAnswer();
  	 	}else{
  	 		WrongAnswer();
  	 	}
  	 	
   	}
   	
   	void CorrectAnswer(){
   		currentGame++;
   		if(currentGame <=TOTAL_GAMES){
   			Debug.Log(currentGame);
  			RestartGame();
   		}else{
   			EndGame();
   		}
   	}
   	
   	void WrongAnswer(){
   		currentGame++;
   		if(currentGame <=TOTAL_GAMES){
  	 		RestartGame();
  	 	}else{
   			EndGame();
   		}
   	}
   	
   	void EndGame(){
   	
   	}
}
